#pragma once

#include <iostream>

#ifdef SampleNumber_EXPORTS
	#define SampleNumber_API __declspec(dllexport)
#else
	#define SampleNumber_API __declspec(dllimport)
#endif

class SampleNumber_API SampleNumber
{
	public:
		//We actually don't need to instantiate this
		static int getCount(const int&, const int&);
};