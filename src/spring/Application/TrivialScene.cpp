#include "..\..\..\include\spring\Application\TrivialScene.h"

namespace Spring
{
	TrivialScene::TrivialScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void TrivialScene::createScene()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 2, 1, 1, 1);

		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));

		gridLayout->addWidget(label, 1, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_2, 0, 1, 1, 1);


		gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);
		menuBar = new QMenuBar(m_uMainWindow.get());
		menuBar->setObjectName(QStringLiteral("menuBar"));
		menuBar->setGeometry(QRect(0, 0, 699, 25));
		m_uMainWindow.get()->setMenuBar(menuBar);
		statusBar = new QStatusBar(m_uMainWindow.get());
		statusBar->setObjectName(QStringLiteral("statusBar"));
		m_uMainWindow.get()->setStatusBar(statusBar);

		m_uMainWindow.get()->setWindowTitle(QApplication::translate("MainWindow", "DisplayWindow", Q_NULLPTR));
		label->setText(QString::number(SampleNumber::getCount(5, 6)));
	}

	void TrivialScene::release()
	{
		delete centralWidget;
	}

	TrivialScene::~TrivialScene()
	{
	}
}
