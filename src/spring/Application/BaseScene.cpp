#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		createGUI();

		QObject::connect(backButton, SIGNAL(released()), this, SLOT(mf_BackButton()));
	}

	void  BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 4, 0, 1, 1);

		startButton = new QPushButton(centralWidget);
		startButton->setObjectName(QStringLiteral("startButton"));

		gridLayout->addWidget(startButton, 4, 1, 1, 1);

		stopButton = new QPushButton(centralWidget);
		stopButton->setObjectName(QStringLiteral("stopButton"));

		gridLayout->addWidget(stopButton, 4, 2, 1, 1);

		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("backButton"));

		gridLayout->addWidget(backButton, 4, 3, 1, 1);

		listView = new QCustomPlot(centralWidget);
		listView->setObjectName(QStringLiteral("listView"));

		gridLayout->addWidget(listView, 0, 0, 1, 4);


		gridLayout_2->addLayout(gridLayout, 0, 1, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);
		menuBar = new QMenuBar(m_uMainWindow.get());
		menuBar->setObjectName(QStringLiteral("menuBar"));
		menuBar->setGeometry(QRect(0, 0, 699, 26));
		m_uMainWindow.get()->setMenuBar(menuBar);
		statusBar = new QStatusBar(m_uMainWindow.get());
		statusBar->setObjectName(QStringLiteral("statusBar"));
		m_uMainWindow.get()->setStatusBar(statusBar);

		stopButton->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
		startButton->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
		backButton->setText(QApplication::translate("MainWindow", "Back", Q_NULLPTR));
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}
	
	BaseScene::~BaseScene()
	{
	}

	void BaseScene::mf_BackButton()
	{
		const std::string initialSceneName = "InitialScene";
		emit SceneChange(initialSceneName);
	}
}
