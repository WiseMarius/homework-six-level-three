#pragma once
#include <spring\Framework\IScene.h>
#include <SampleNumber.h>

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

namespace Spring
{
	class TrivialScene : public IScene
	{
		Q_OBJECT
	public:

		explicit TrivialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~TrivialScene();

	private:
		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QSpacerItem *verticalSpacer;
		QLabel *label;
		QSpacerItem *horizontalSpacer;
		QSpacerItem *horizontalSpacer_2;
		QSpacerItem *verticalSpacer_2;
		QMenuBar *menuBar;
		QStatusBar *statusBar;
	};

}
